<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <link rel='stylesheet' href='resources/css/bootstrap.min.css' type='text/css' media='all'>
    <title>Title</title>
</head>

<body>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.14/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.14/angular-resource.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.14/angular-route.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.14/angular-cookies.js"></script>

<script src="resources/js/app.js"></script>
<script src="resources/js/loginController.js"></script>
<script src="resources/js/navbarController.js"></script>
<script src="resources/js/userController.js"></script>
<script src="resources/js/userUtilsController.js"></script>
<script src="resources/js/accidentController.js"></script>
<script src="resources/js/injuredController.js"></script>
<script src="resources/js/subscriptionController.js"></script>

<div ng-controller="NavBarController as navbarCtrl" >
    <ul class="breadcrumb">
        <li><a ng-href="#/user">Home</a></li>
        <li ng-show="navbarCtrl.isLogined()" ><a href ng-click="navbarCtrl.logout()">Log out</a></li>
    </ul>
</div>

<div ng-view></div>
</body>
</html>
