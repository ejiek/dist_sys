/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function NavBarController ($scope, $cookies) {
    this.isLogined = function() {
        return $cookies.getObject('user') != null
    };

    this.logout = function() {
        $cookies.remove('user');
        window.location.href = '#/login';
    }
}

app.controller('NavBarController', NavBarController);
