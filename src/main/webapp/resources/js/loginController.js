/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function isEmpty(str) {
    return (!str || 0 === str.length);
}

function LoginController($scope, $http, $cookies, UserService) {
    if ($cookies.getObject('user') != null) {
        window.location.href = '#/user';
    }
    this.isRegister = false;
    this.signIn = function () {
        if (!$scope.login) {
            alert("Enter login");
        } else if (!$scope.passwd) {
            alert("Enter password");
        } else {
            $http.get('rest/user/' + $scope.login + '/authenticate?passwd=' + $scope.passwd)
                .then(function (responce) {
                    if (responce.data.toString() == "true") {
                        UserService.get({login:$scope.login}, function(data){
                            $cookies.putObject('user', data);
                            window.location.href = '#/user';
                        });
                    } else {
                        alert("Incorrect login or password");
                        $scope.login = "";
                        $scope.passwd = "";
                    }
                }, function (error) {
                    alert("Incorrect login or password");
                    $scope.login = "";
                    $scope.passwd = "";
                });
        }
    };

    this.registerUser = function () {
        if (isEmpty($scope.loginReg)) {
            console.log($scope.loginReg);
            alert("Enter the login");
        } else if (isEmpty($scope.password1)) {
            alert("Enter the password");
        } else if ($scope.password1 != $scope.password2) {
            alert("Passwords should be equal");
        } else if (isEmpty($scope.userType)) {
            alert("Choose your destiny, please!")
        }
         else {
            var user = new UserService();
            user.login = $scope.loginReg;
            user.password = $scope.password1;
            user.type = $scope.userType;
            user.$save({login:''}, function () {
                $scope.loginReg = "";
                $scope.password1 = "";
                $scope.password2 = "";
                $scope.userType = "";
                this.isRegister = false;
            }.bind(this), function (error) {
                alert(error.data.message);
            });
        }
    }.bind(this);

    $http.get('rest/enum/user_types').success( function(userTypes){
        $scope.userTypes = userTypes;
    })

}

app
    .controller('LoginController', LoginController);

