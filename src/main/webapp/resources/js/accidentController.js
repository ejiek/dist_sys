/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function AccidentService($resource) {
    return $resource('rest/accident/:id', { id: '@id' });
}

function AccidentInjuredService($resource) {
    return $resource('rest/accident/:id/injured', { id: '@id' });
}

function InjuredRegistrationService($resource) {
    return $resource('rest/injured?accidentId=:id', {id: '@id'});
}

function AccidentController($scope, $http, $routeParams, $cookies,
                        AccidentService,
                        InjuredRegistrationService,
                        InjuredService,
                        AccidentInjuredService) {
    var url = function () {
        return {id:$routeParams.id};
    };

    if ($cookies.getObject('user') == null) {
        window.location.href = '#/login';
    }

    this.instance = AccidentService.get({id:$routeParams.id}, GetInjuredToScope());

    this.isAddMenuVisible = false;

    this.isNew = function () {
        return (this.instance.type == 'NEW');
    };

    this.isInprogress = function () {
        return (this.instance.type == 'INPROGRESS');
    };
    this.isFinished = function () {
        return (this.instance.type == 'FINISHED');
    };

    this.injuredSelect = function(injured) {
        if ($scope.injuredSelected == injured) {
            $http.get('rest/injured/' + injured.id )
                .then(function (responce) {
                    window.location.href = '#/injured/' + responce.data.id;
                });
        }
        $scope.injuredSelected = injured;
    };

    this.registerInjured = function() {
        if (isEmpty($scope.name)) {
            alert("Enter name");
        } else if (isEmpty($scope.age)) {
            alert("Enter age");
        } else if (isEmpty($scope.gender)) {
            alert("Choose your gender, please!")
        }
        else {
            var injured = new InjuredRegistrationService();
            injured.name = $scope.name;
            injured.age = $scope.age;
            injured.insurance = $scope.insurance;
            injured.gender = $scope.gender;
            injured.$save(url()).then( function () {
                $scope.name = "";
                $scope.age = "";
                $scope.insurance = "";
                $scope.gender = "";
                isAddMenuVisible = false;
                AccidentInjuredService.query({id:$routeParams.id}, GetInjuredToScope());
            }, function (error) {
                alert(error.data.message);
            });
        }
    };


    $http.get('rest/enum/genders').success(function(genders){
        $scope.genders = genders;
    })

	var isAddMenuVisible = false;

	this.isAddMenuVisible = function () {
	    return isAddMenuVisible;
	}

    this.setVisible = function (){
       isAddMenuVisible = !isAddMenuVisible;
    }

	function GetInjuredToScope() {
        $scope.injured = AccidentInjuredService.query({id:$routeParams.id}, function(injured_list){
            $scope.injured = [];
            angular.forEach(injured_list, function (value) {
                $scope.injured.push(InjuredService.get({id:value}))
            })
        });
	};
}

app
    .factory('AccidentService', AccidentService)
    .factory('AccidentInjuredService', AccidentInjuredService)
    .factory('InjuredRegistrationService', InjuredRegistrationService)
    .controller('AccidentController', AccidentController);
