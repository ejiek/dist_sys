/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function SubscriptionService($resource) {
    return $resource('rest/sub/:id', { id: '@id' });
}

function SubscriptionAccidentsService($resource) {
    return $resource('rest/sub/:id/accidents', { id: '@id' });
}

function SubscriptionUnsubscribeService($resource) {
    return $resource('rest/sub/:id/unsubscribe', { id: '@id' });
}

function SubscriptionController($scope, $http, $routeParams, $cookies,
                        AccidentService,
                        SubscriptionAccidentsService,
                        SubscriptionUnsubscribeService,
                        SubscriptionService) {
    if ($cookies.getObject('user') == null) {
        window.location.href = '#/login';
    }

    this.instance = SubscriptionService.get({id:$routeParams.id}, GetAccidentsToScope());

    this.accidentSelect = function(accident) {
        if ($scope.accidentSelected == accident) {
            $http.get('rest/accident/' + accident.id )
                .then(function (responce) {
                    window.location.href = '#/accident/' + responce.data.id;
                });
        }
        $scope.accidentSelected = accident;
    };

    this.unsubscribe = function() {
        SubscriptionUnsubscribeService.get({id:$routeParams.id}, function(response) {
             window.location.href = '#/user';
        }, function(error){
			console.log('subscribe error: ', error);
		    alert('Unable to unsubscrbe');
		})
    };

    function GetAccidentsToScope() {
        SubscriptionAccidentsService.query({id:$routeParams.id}, function(accident_list){
            $scope.accidents = [];
            angular.forEach(accident_list, function (value) {
                $scope.accidents.push(AccidentService.get({id:value}))
            })
        });
    }
}

app
    .factory('SubscriptionService', SubscriptionService)
    .factory('SubscriptionAccidentsService', SubscriptionAccidentsService)
    .factory('SubscriptionUnsubscribeService', SubscriptionUnsubscribeService)
    .controller('SubscriptionController', SubscriptionController);
