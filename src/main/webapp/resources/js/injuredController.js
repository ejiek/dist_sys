/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function InjuredService($resource) {
    return $resource('rest/injured/:id', { id: '@id' });
}

function InjuredAcceptService($resource) {
    return $resource('rest/injured/:id/accept?userId=:userId', { id: '@id', userId: '@userId' });
}

function InjuredMedicsService($resource) {
    return $resource('rest/user/:login/medics', { login: '@login' });
}

function  InjuredUpdateService($resource) {
    return $resource('rest/injured', null,
        {
            'update': { method:'PUT'}
        });
}

function HealthUpdateService($resource) {
    return $resource('rest/health/:id', { id: '@id' });
}

function InjuredHealthUpdateService($resource) {
    return $resource('rest/injured/:id/updates', { id: '@id' });
}

function InjuredDischargementService($resource) {
    return $resource('rest/injured/:id/discharge', { id: '@id' });
}

function InjuredController($scope, $http, $routeParams, $cookies,
                        InjuredService,
                        InjuredUpdateService,
                        InjuredAcceptService,
                        InjuredMedicsService,
                        InjuredHealthUpdateService,
                        HealthUpdateService,
                        UserService,
                        InjuredDischargementService ) {
    var url = function () {
        return {id: $routeParams.id};
    };

    if ($cookies.getObject('user') == null) {
        window.location.href = '#/login';
    }

    InjuredService.get(url(), function (value) {
        $scope.gender = value.gender;
        $scope.age = value.age;
        $scope.name = value.name;
        $scope.insurance = value.insurance;
        $scope.medicIdSelected = value.medicId;
        $scope.status = value.status;
        $scope.injured = value;
    });

    if ($cookies.getObject('user').type == 'OPERATOR') {
        InjuredMedicsService.query({login: $cookies.getObject('user').login}, function (medics_list) {
            $scope.medics = [];
            angular.forEach(medics_list, function (value) {
                $scope.medics.push(UserService.get({id: value}))
            })
        });
    }

    if ($cookies.getObject('user').type == 'MEDIC' || $cookies.getObject('user').type == 'REGULAR') {
        GetHealthUpdatesToScope();
    }

    this.updateInjured = function () {
        if (isEmpty($scope.name)) {
            alert("Enter name");
        } else if (isEmpty($scope.age)) {
            alert("Enter age");
        } else if (isEmpty($scope.gender)) {
            alert("Choose gender of the injured person, please!")
        }
        else {
            injured = $scope.injured;
            injured.name = $scope.name;
            injured.age = $scope.age;
            injured.insurance = $scope.insurance;
            injured.gender = $scope.gender;
            injured.medicId = $scope.medicIdSelected;
            InjuredUpdateService.update(url(), injured, function () {
                InjuredService.get({id: $scope.injured.id}, function (data) {
                    alert("Injured Updated");
                    $scope.injured = data;
                    $scope.gender = data.gender;
                    $scope.age = data.age;
                    $scope.name = data.name;
                    $scope.insurance = data.insurance;
                    $scope.medicIdSelected = data.medicId;
                    $scope.status = data.status;
                });
            }, function (error) {
                alert(error.data.message);
            });
        }
    };

    this.medicSelect = function (medic) {
        $scope.medicIdSelected = medic.id;
    };

    $http.get('rest/enum/genders').success(function (genders) {
        $scope.genders = genders;
    });

    this.isNew = function () {
        return ($scope.status == 'NEW');
    };

    this.isOnMedication = function () {
        return ($scope.status == 'ONMEDICATION');
    };

    this.setVisible = function () {
        this.isAddMenuVisible = !this.isAddMenuVisible;
    };

    this.accept = function () {
        InjuredAcceptService.get(url(), function (data) {
            InjuredService.get({id: $scope.injured.id, userId: $cookies.getObject('user').id}, function (data) {
                alert("Injured accepted");
                $scope.injured = data;
                $scope.gender = data.gender;
                $scope.age = data.age;
                $scope.name = data.name;
                $scope.insurance = data.insurance;
                $scope.medicIdSelected = data.medicId;
                $scope.status = data.status;
            });
        })
    };

    this.registerUpdate = function (scopeUpdate) {
        var update = new HealthUpdateService();
        update.text = scopeUpdate;
        update.injuredId = $scope.injured.id;
        update.$save({injuredId: $scope.injured.id}, function (data) {
            GetHealthUpdatesToScope();
            $scope.update = "";
        });
    };

    this.discharge = function () {
        InjuredDischargementService.query(url(), function (data) {
            GetHealthUpdatesToScope();
            InjuredService.get({id: $scope.injured.id, userId: $cookies.getObject('user').id}, function (data) {
                $scope.status = data.status;
            });
        });
    };

    function GetHealthUpdatesToScope(healthupdate_list) {
        InjuredHealthUpdateService.query({id: $routeParams.id}, function (healthupdate_list) {
            $scope.healthUpdates = [];
            angular.forEach(healthupdate_list, function (value) {
                $scope.healthUpdates.push(HealthUpdateService.get({id: value}))
            })
        });
    }
}

app
    .factory('InjuredService', InjuredService)
    .factory('InjuredUpdateService', InjuredUpdateService)
    .factory('InjuredAcceptService', InjuredAcceptService)
    .factory('InjuredMedicsService', InjuredMedicsService)
    .factory('HealthUpdateService', HealthUpdateService)
    .factory('InjuredHealthUpdateService', InjuredHealthUpdateService)
    .factory('InjuredDischargementService', InjuredDischargementService)
    .controller('InjuredController', InjuredController);
