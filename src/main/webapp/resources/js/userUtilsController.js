/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function UserUtilsController($cookies) {
    this.isMedic = function () {
        return ($cookies.getObject('user').type == 'MEDIC');
    };

    this.isOperator = function () {
        return ($cookies.getObject('user').type == 'OPERATOR');
    };

    this.isRegular = function () {
        return ($cookies.getObject('user').type == 'REGULAR');
    };
}

app
    .controller('UserUtilsController', UserUtilsController);
