/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
function UserService($resource) {
    return $resource('rest/user/:login', { login: '@login' });
}

function UserSubscriptionService($resource) {
    return $resource('rest/user/:login/subscriptions', { login: '@login' });
}

function UserRequestsService($resource) {
    return $resource('rest/user/:login/requests', {login: '@login' });
}

function RequestService($resource) {
    return $resource('rest/request/:id', { id: '@id' });
}

function UserAccidentsService($resource) {
    return $resource('rest/user/:login/accidents', {login: '@login'});
}

function UserInjuredService($resource) {
    return $resource('rest/user/:login/injured', {login: '@login'});
}


function UserController($scope, $http, $routeParams, $cookies,
                        UserService,
                        UserSubscriptionService,
						RequestService,
                        UserRequestsService,
                        InjuredService,
                        SubscriptionService,
                        UserInjuredService) {
    var url = function () {
        return {login:$routeParams.login};
    };

    if ($cookies.getObject('user') == null) {
        window.location.href = '#/login';
    }

    this.instance = $cookies.getObject('user');

    if (this.instance.type == 'OPERATOR') {
        GetRequestsToScope($scope, $cookies);
    }
    if (this.instance.type == 'MEDIC') {
        UserInjuredService.query({login:$cookies.getObject('user').login}, function(injured_list){
   	       $scope.injured = [];
   	       angular.forEach(injured_list, function (value) {
       	       $scope.injured.push(InjuredService.get({id:value}))
       	   })
        });
    }

    if (this.instance.type == 'REGULAR') {
        UserSubscriptionService.query({login:$cookies.getObject('user').login},  function(subscription_list){
            $scope.subscriptions = [];
            angular.forEach(subscription_list, function (value) {
                $scope.subscriptions.push(SubscriptionService.get({id:value}))
            })
        });
    }

    this.isMedic = function () {
        return (this.instance.type == 'MEDIC');
    };

    this.isOperator = function () {
        return (this.instance.type == 'OPERATOR');
    };
    this.isRegular = function () {
        return (this.instance.type == 'REGULAR');
    };

    this.requestSelect = function(request) {
        if ($scope.requestSelected !== 'undefined'){
            if ($scope.requestSelected == request && request.status == 'ACCEPTED') {
                $http.get('rest/accident/?requestId=' + request.id )
                    .then(function (responce) {
                        window.location.href = '#/accident/' + responce.data.id;
                    });
            }
        }
        $scope.requestSelected = request;
    };

    this.acceptRequest = function() {
        $http.get('rest/request/' + $scope.requestSelected.id + '/accept').then(
            GetRequestsToScope($scope, $cookies)
        );
    };

    this.declineRequest = function() {
        $http.get('rest/request/' + $scope.requestSelected.id + '/decline').then(
            GetRequestsToScope($scope, $cookies)
        );
    };

    this.injuredSelect = function(injured) {
        if ($scope.injuredSelected == injured) {
            $http.get('rest/injured/' + injured.id )
                .then(function (responce) {
                    window.location.href = '#/injured/' + responce.data.id;
                });
        }
        $scope.injuredSelected = injured;
    };

    this.subscriptionSelect = function(subscription) {
        if ($scope.subscriptionSelected == subscription) {
            $http.get('rest/sub/' + subscription.id )
                .then(function (responce) {
                    window.location.href = '#/subscription/' + responce.data.id;
                });
        }
        $scope.subscriptionSelected = subscription;
    };

    this.addSubscription = function() {
        var subscription = new SubscriptionService();
        subscription.vin = $scope.subscription;
        subscription.userId = $cookies.getObject('user').id;
        subscription.$save('', function(){
		    GetSubscriptionsToScope();
			$scope.subscription = "";
		});
    };

    function GetSubscriptionsToScope() {
        UserSubscriptionService.query({login:$cookies.getObject('user').login}, function(subscription_list){
            $scope.subscriptions = [];
            angular.forEach(subscription_list, function (value) {
                $scope.subscriptions.push(SubscriptionService.get({id:value}))
            })
        });
    }

    function GetRequestsToScope() {
        UserRequestsService.query({login:$cookies.getObject('user').login}, function(request_list){
            $scope.requests = [];
            angular.forEach(request_list, function (value) {
                $scope.requests.push(RequestService.get({id:value}))
            })
        });
    }

}

app
    .factory('UserService', UserService)
    .factory('UserSubscriptionService', UserSubscriptionService)
    .factory('UserRequestsService', UserRequestsService)
    .factory('UserInjuredService', UserInjuredService)
    .factory('RequestService', RequestService)
    .controller('UserController', UserController);
