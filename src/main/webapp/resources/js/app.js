/**
 * Copyright (c) 2018 ejiek
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var app = angular.module('app', ['ngRoute','ngResource','ngCookies']);
app.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/', {
            redirectTo: '/login'
        })
        .when('/login', {
            templateUrl: 'view/loginPage.html',
            controller: 'LoginController',
            controllerAs: 'loginCtrl'
        })
        .when('/user', {
            templateUrl: 'view/userPage.html',
            controller: 'UserController',
            controllerAs: 'userCtrl'
        })
        .when('/subscription/:id', {
            templateUrl: 'view/subscriptionPage.html',
            controller: 'SubscriptionController',
            controllerAs: 'subCtrl'
        })
        .when('/accident/:id', {
            templateUrl: 'view/accidentPage.html',
            controller: 'AccidentController',
            controllerAs: 'accidentCtrl'
        })
        .when('/injured/:id', {
            templateUrl: 'view/injuredPage.html',
            controller: 'InjuredController',
            controllerAs: 'injuredCtrl'
        })
        .otherwise(
            { redirectTo: '/'}
        );
}]);
