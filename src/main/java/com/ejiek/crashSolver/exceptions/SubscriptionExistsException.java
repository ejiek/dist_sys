package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/18/17.
 */
public class SubscriptionExistsException extends Exception {
    public SubscriptionExistsException(String vin) {
        super("Subscription for vin " + vin + "already exists");
    }
}
