package com.ejiek.crashSolver.exceptions;

public class WrongPasswordException extends Exception {
    public WrongPasswordException(String user) {
        super ("Attempt to login with wrong password to user " + user);
    }
}
