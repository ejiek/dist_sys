package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/18/17.
 */
public class NoInjuredFoundException extends NoInstancesFoundException {
    public NoInjuredFoundException() {
        super ("No injured found");
    }
}
