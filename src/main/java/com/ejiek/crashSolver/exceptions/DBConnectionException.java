package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/12/17.
 */
public class DBConnectionException extends Exception {
    public DBConnectionException() {
        super("Unable to connect to the database");
    }
}
