package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/18/17.
 */
abstract public class NoInstancesFoundException extends Exception {
    public NoInstancesFoundException(String message) {
        super (message);
    }
}
