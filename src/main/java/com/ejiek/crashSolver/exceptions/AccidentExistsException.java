package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/18/17.
 */
public class AccidentExistsException extends Exception {
    public AccidentExistsException(String accident) {
        super("Accident for request " + accident + "already exists");
    }
}
