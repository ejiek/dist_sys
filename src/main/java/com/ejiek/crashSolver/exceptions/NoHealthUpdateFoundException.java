package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/18/17.
 */
public class NoHealthUpdateFoundException extends NoInstancesFoundException {
    public NoHealthUpdateFoundException() {
        super ("No health update found");
    }
}
