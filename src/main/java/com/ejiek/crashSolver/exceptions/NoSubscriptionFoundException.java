package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/18/17.
 */
public class NoSubscriptionFoundException extends NoInstancesFoundException{
    public NoSubscriptionFoundException() {
        super ("No subscription found");
    }
}
