package com.ejiek.crashSolver.exceptions;

public class NotAuthenticatedException extends Exception  {
    public NotAuthenticatedException(String user) {
        super(user + " is not authenticated");
    }
}
