package com.ejiek.crashSolver.exceptions;

/**
 * Created by ejiek on 6/18/17.
 */
public class NoRequestsFoundException extends NoInstancesFoundException {
    public NoRequestsFoundException() {
        super ("No request were found");
    }
}
