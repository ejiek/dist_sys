/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.DTO;

import com.ejiek.crashSolver.entity.Gender;
import com.ejiek.crashSolver.entity.Injured;
import com.ejiek.crashSolver.entity.InjuredStatus;
import com.ejiek.crashSolver.entity.User;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.exceptions.WrongUserTypeException;
import com.ejiek.crashSolver.repository.UserRepository;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;

public class InjuredDto {
    @NotNull
    private String name;

    private Long id;

    @NotNull
    private int age;

    private int insurance;

    private Long medicId;

    @NotNull
    private Gender gender;

    private InjuredStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getInsurance() {
        return insurance;
    }

    public void setInsurance(int insurance) {
        this.insurance = insurance;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setStatus(InjuredStatus status) {
        this.status = status;
    }

    public InjuredStatus getStatus() {
        return status;
    }

    public Long getMedicId() {
        return medicId;
    }

    public void setMedicId(Long medicId) {
        this.medicId = medicId;
    }

    public User getMedicConverted(UserRepository userRepository) {
        if (medicId != null) {
            User user = userRepository.findById(medicId).orElseThrow(() -> new NotFoundException("id" + medicId));
            if (!user.isMedic()) throw new WrongUserTypeException(user.getType().toString());
            return user;
        } else { return null; }
    }

    public void setMedic(User medic) {
        if (medic != null) {
            this.medicId = medic.getId();
        } else {
            medicId = null;
        }
    }

}
