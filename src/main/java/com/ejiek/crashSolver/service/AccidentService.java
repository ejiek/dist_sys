/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.service;

import com.ejiek.crashSolver.entity.Accident;
import com.ejiek.crashSolver.entity.Request;
import com.ejiek.crashSolver.entity.User;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.repository.AccidentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class AccidentService {
    @Autowired
    AccidentRepository accidentRepository;

    public void create(Request request) {
        Accident accident = new Accident();
        accident.setRequest(request);
        accidentRepository.save(accident);
    }

    public Accident findById(Long id) {
        return accidentRepository.findById(id).orElseThrow(() -> new NotFoundException("Accident " + id));
    }

    public Accident findByRequest(Request request) {
        return accidentRepository.findByRequestId(request.getId()).orElseThrow(() -> new NotFoundException("Accident for request: " + request.getId()));
    }

    public Collection<Accident> findByVin(String vin) {
        return accidentRepository.findByRequestVin(vin);
    }

    public Collection<Accident> findByOperator(User user) {
        return accidentRepository.findByOperatorLogin(user.getLogin());
    }
}
