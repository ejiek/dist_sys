/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.service;

import com.ejiek.crashSolver.DTO.InjuredDto;
import com.ejiek.crashSolver.entity.*;
import com.ejiek.crashSolver.exceptions.IncorrectInjuredStatusException;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.repository.AccidentRepository;
import com.ejiek.crashSolver.repository.HealthUpdateRepository;
import com.ejiek.crashSolver.repository.InjuredRepository;
import com.ejiek.crashSolver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class InjuredService {
    @Autowired
    InjuredRepository injuredRepository;
    @Autowired
    HealthUpdateRepository healthUpdateRepository;
    @Autowired
    AccidentRepository accidentRepository;
    @Autowired
    UserRepository userRepository;

    public Injured findById(Long id){
        return injuredRepository.findById(id).orElseThrow(() -> new NotFoundException("Injured " + id));
    }

    public void register(Injured injured, long accidentId) {
        injured.setStatus(InjuredStatus.NEW);
        injured.setAccident(accidentRepository.findById(accidentId).orElseThrow(()
                -> new NotFoundException("Accident to assign injured to " + accidentId)));
        injuredRepository.save(injured);
    }

    public void update(Injured injured) {
        Injured oldInjured = injuredRepository.findById(injured.getId()).orElseThrow(()
                -> new NotFoundException("Injured" + injured.getId()));

        oldInjured.setMedic(injured.getMedic());
        oldInjured.setAge(injured.getAge());
        oldInjured.setGender(injured.getGender());
        oldInjured.setInsurance(injured.getInsurance());
        oldInjured.setName(injured.getName());

        injuredRepository.save(oldInjured);
    }

    public void discharge(Injured injured) {
        injured.setStatus(InjuredStatus.DISCHARGED);
        injuredRepository.save(injured);
        HealthUpdate healthUpdate = new HealthUpdate();
        healthUpdate.setInjured(injured);
        healthUpdate.setText("Discharged");
        healthUpdateRepository.save(healthUpdate);
    }

    public void accept(Injured injured) {
        if(!injured.getStatus().equals(InjuredStatus.NEW))
            throw new IncorrectInjuredStatusException(injured.getId().toString());
        injured.setStatus(InjuredStatus.ONMEDICATION);
        injuredRepository.save(injured);
    }

    public Injured getInjuredById(Long id){
        return injuredRepository.findById(id).orElseThrow(() -> new NotFoundException("id: " + id));
    }

    public Collection<HealthUpdate> healthUpdates(Long id) {
        Injured injured = injuredRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Injured " + id));
        return healthUpdateRepository.findByInjuredId(injured.getId());
    }

    public Collection<Injured> findByMedic(User medic) {
        return injuredRepository.findByMedicId(medic.getId());
    }

    public Collection<Injured> findByAccident(Accident accident) {
        return injuredRepository.findByAccidentId(accident.getId());
    }
}
