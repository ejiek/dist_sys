/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.service;

import com.ejiek.crashSolver.entity.Subscription;
import com.ejiek.crashSolver.entity.User;
import com.ejiek.crashSolver.entity.Accident;
import com.ejiek.crashSolver.exceptions.ExistsException;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.repository.SubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SubscriptionService {
    @Autowired
    SubscriptionRepository subscriptionRepository;
    @Autowired
    UserService userService;
    @Autowired
    AccidentService accidentService;

    public Subscription findById(Long id) {
        return subscriptionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Subscription " + id));
    }

    public Collection<Subscription> findByUser(User user) {
        return subscriptionRepository.findByUserId(user.getId());
    }

    public void register(Subscription subscription) {
        for (Subscription s:subscriptionRepository.findByUserLogin(subscription.getUser().getLogin())) {
            if( s.getVin().equals(subscription.getVin()) )
                throw new ExistsException("Subscription" + subscription.getVin());
        }
        subscriptionRepository.save(subscription);
    }

	public Collection<Accident> getAccidents(Subscription subscription) {
		return accidentService.findByVin(subscription.getVin());
	}

    public void delete(Subscription subscription) {
        subscriptionRepository.delete(subscription);
    }

}
