/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.service;

import com.ejiek.crashSolver.entity.*;
import com.ejiek.crashSolver.exceptions.*;
import com.ejiek.crashSolver.repository.SubscriptionRepository;
import com.ejiek.crashSolver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    SubscriptionService subscriptionService;
    @Autowired
    RequestService requestService;
    @Autowired
    InjuredService injuredService;
    @Autowired
    AccidentService accidentService;

    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException("User " + id));
    }

    public User findByLogin(String login) {
        return userRepository.findByLogin(login).orElseThrow(() -> new NotFoundException("User " + login));
    }

    public void register(User user) {
        if (userRepository.findByLogin(user.getLogin()).isPresent())
            throw new ExistsException("User " + user.getLogin());
        notNull(user.getLogin());
        notNull(user.getPassword());
        notNull(user.getType());
        user.setId(null);
        userRepository.save(user);
    }

    public void authenticate(User user, String password) {
        if (!user.getPassword().equals(password))
            throw new AuthenticationException();
    }

    public Collection<User> findMedics(){
        return userRepository.findByType(UserType.MEDIC);
    }

    public Collection<Subscription> getSubscriptions(User user) {
        if (!user.isRegular()) throw new NotAllowed(user.getType() + " is not allowed to get subscriptions");
        return subscriptionService.findByUser(user);
    }

    public Collection<Request> getRequests(User user) {
        if (!user.isOperator()) throw new NotAllowed(user.getType() + " is not allowed to get subscriptions");
        return requestService.findAll();
    }

    public Collection<Injured> getInjured(User user) {
        if (!user.isMedic()) throw new NotAllowed(user.getType() + " is not allowed to get subscriptions");
        return injuredService.findByMedic(user);
    }

    public Collection<Accident> getAccident(User user) {
        if (!user.isMedic() && !user.isOperator()) throw new NotAllowed(user.getType() + " is not allowed to get subscriptions");
        return accidentService.findByOperator(user);
    }

    private void notNull(Object o){
        if ( o == null ) throw new NotEnoughArgumentsException("request");
    }
}
