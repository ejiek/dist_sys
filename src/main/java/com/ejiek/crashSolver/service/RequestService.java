/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.service;

import com.ejiek.crashSolver.entity.Accident;
import com.ejiek.crashSolver.entity.Request;
import com.ejiek.crashSolver.entity.RequestStatus;
import com.ejiek.crashSolver.exceptions.IncorrectRequestStatusException;
import com.ejiek.crashSolver.exceptions.NotEnoughArgumentsException;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.repository.AccidentRepository;
import com.ejiek.crashSolver.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

@Service
public class RequestService {
    @Autowired
    RequestRepository requestRepository;
    @Autowired
    AccidentService accidentService;

    public Request findById(Long id) {
        return requestRepository.findById(id).orElseThrow(() -> new NotFoundException("Request " + id));
    }

    public void register(Request request) {
        notNull(request.getVin());
        notNull(request.getBelted());
        notNull(request.getColor());
        notNull(request.getSpeed());
        request.setStatus(RequestStatus.NEW);
        request.setTimeStamp(new Date());
        request.setId(null);
        requestRepository.save(request);
    }

    public void accept(Request request) {
        checkStatus(request, RequestStatus.NEW);
        accidentService.create(request);
        request.setStatus(RequestStatus.ACCEPTED);
        requestRepository.save(request);
    }

    public void decline(Request request) {
        checkStatus(request, RequestStatus.NEW);
        request.setStatus(RequestStatus.DECLINED);
        requestRepository.save(request);
    }

    public Collection<Request> findAll(){
        return requestRepository.findAllByOrderByIdAsc();
    }

    private void notNull(Object o){
        if ( o == null ) throw new NotEnoughArgumentsException("request");
    }

    private void checkStatus(Request request, RequestStatus status) {
        if (request.getStatus() != status) throw new IncorrectRequestStatusException(request.getId().toString());
    }
}
