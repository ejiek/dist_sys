/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.controller;

import com.ejiek.crashSolver.DTO.HealthUpdateDto;
import com.ejiek.crashSolver.entity.HealthUpdate;
import com.ejiek.crashSolver.exceptions.ParseException;
import com.ejiek.crashSolver.repository.HealthUpdateRepository;
import com.ejiek.crashSolver.service.HealthUpdateService;
import com.ejiek.crashSolver.service.InjuredService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class HealthUpdateController {
    @Autowired
    HealthUpdateRepository healthUpdateRepository;

    @Autowired
    InjuredService injuredService;
    @Autowired
    HealthUpdateService healthUpdateService;
    @Autowired
    ModelMapper modelMapper;

    @RequestMapping("rest/health/{id}")
    public HealthUpdateDto getHealthUpdate(@PathVariable Long id) {
        return convertToDto(healthUpdateService.findById(id));
    }

    @RequestMapping(value = "rest/health", method = { RequestMethod.GET, RequestMethod.POST })
    public void registerHealthUpdate( @RequestParam("injuredId") Long injuredId,
                                 @RequestBody HealthUpdateDto healthUpdateDto) {
        healthUpdateService.register(convertToEntity(healthUpdateDto));
    }

    private HealthUpdateDto convertToDto(HealthUpdate healthUpdate) {
        HealthUpdateDto healthUpdateDto = modelMapper.map(healthUpdate, HealthUpdateDto.class);
        healthUpdateDto.setInjuredId(healthUpdate.getInjured().getId());
        return healthUpdateDto;
    }

    private HealthUpdate convertToEntity(HealthUpdateDto healthUpdateDto) throws ParseException {
        HealthUpdate healthUpdate = modelMapper.map(healthUpdateDto, HealthUpdate.class);
        healthUpdate.setInjured(injuredService.findById(healthUpdateDto.getInjuredId()));
        return healthUpdate;
    }
}
