/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.controller;

import com.ejiek.crashSolver.DTO.InjuredDto;
import com.ejiek.crashSolver.DTO.RequestDto;
import com.ejiek.crashSolver.entity.Accident;
import com.ejiek.crashSolver.entity.Injured;
import com.ejiek.crashSolver.entity.Request;
import com.ejiek.crashSolver.entity.RequestStatus;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.exceptions.ParseException;
import com.ejiek.crashSolver.repository.AccidentRepository;
import com.ejiek.crashSolver.repository.RequestRepository;
import com.ejiek.crashSolver.service.RequestService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    @Autowired
    RequestRepository requestRepository;
    @Autowired
    AccidentRepository accidentRepository;
    @Autowired
    RequestService requestService;
    @Autowired
    ModelMapper modelMapper;

    @RequestMapping("rest/request/{id}")
    public Request getRequest(@PathVariable Long id) {
        return findById(id);
    }

    @RequestMapping(value = "rest/request/", method = RequestMethod.POST)
    public void register(@RequestBody RequestDto requestDto) {
        requestService.register(convertToEntity(requestDto));
    }

    @RequestMapping(value="rest/request/{id}/accept")
    public void accept(@PathVariable Long id){
        requestService.accept(findById(id));
    }

    @RequestMapping(value="rest/request/{id}/decline")
    public void decline(@PathVariable Long id){
        requestService.decline(findById(id));
    }

    private RequestDto convertToDto(Request request) {
        RequestDto requestDto = modelMapper.map(request, RequestDto.class);
        return requestDto;
    }

    private Request convertToEntity(RequestDto requestDto) throws ParseException {
        Request request = modelMapper.map(requestDto, Request.class);
        return request;
    }

    private Request findById(Long id) {
        return requestRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Request " + id));
    }
}
