/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.controller;

import com.ejiek.crashSolver.DTO.UserDto;
import com.ejiek.crashSolver.entity.*;
import com.ejiek.crashSolver.exceptions.AuthenticationException;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.exceptions.ParseException;
import com.ejiek.crashSolver.repository.*;
import com.ejiek.crashSolver.service.InjuredService;
import com.ejiek.crashSolver.service.RequestService;
import com.ejiek.crashSolver.service.SubscriptionService;
import com.ejiek.crashSolver.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    RequestService requestService;
    @Autowired
    InjuredService injuredService;
    @Autowired
    SubscriptionService subscriptionService;
    @Autowired
    UserService userService;
    @Autowired
    ModelMapper modelMapper;

    @RequestMapping("rest/user/{login}/authenticate")
    public String authenticate(@PathVariable String login,
                               @RequestParam("passwd") String passwd) {
        try {
            User user = userService.findByLogin(login);
            userService.authenticate(user, passwd);
            return "true";
        } catch (NotFoundException user){
            throw new AuthenticationException();
        }
    }

    @RequestMapping("rest/user/{login}")
    public UserDto getUser(@PathVariable String login) {
        return convertToDto(userService.findByLogin(login));
    }

    @RequestMapping("rest/user")
    public UserDto getUser(@RequestParam("id") Long id) {
        return convertToDto(userService.findById(id));
    }

    @RequestMapping(value = "rest/user", method = RequestMethod.POST)
    public void addUser(@RequestBody UserDto userDto) {
        userService.register(convertToEntity(userDto));
    }

    @RequestMapping("rest/user/{login}/subscriptions")
    public List<String> getSubscriptions(@PathVariable String login) {
        User user = userService.findByLogin(login);
        return convertToIdList(userService.getSubscriptions(user));
    }

    @RequestMapping("rest/user/{login}/requests")
    public List<String> getRequests(@PathVariable String login) {
        User user = userService.findByLogin(login);
        return convertToIdList(userService.getRequests(user));
    }

    @RequestMapping("rest/user/{login}/injured")
    public List<String> getInjured(@PathVariable String login) {
        User user = userService.findByLogin(login);
        return convertToIdList(userService.getInjured(user));
    }

    @RequestMapping("rest/user/{login}/accidents")
    public List<String> getAccidents(@PathVariable String login) {
        User user = userService.findByLogin(login);
        return convertToIdList(userService.getAccident(user));
    }

    @RequestMapping("rest/user/{login}/medics")
    public List<String> getMedics(@PathVariable String login) {
        User user = userService.findByLogin(login);
        return convertToIdList(userService.findMedics());
    }

    @RequestMapping(value = "/rest/enum/user_type")
    public List<String> returnActionsStates(){
        List<String> list = new ArrayList<>();
        for (UserType type: UserType.values()){
            list.add(type.toString());
        }
        return list;
    }

    private UserDto convertToDto(User user) {
        UserDto userDto = modelMapper.map(user, UserDto.class);
        userDto.setPassword(null);
        return userDto;
    }

    private User convertToEntity(UserDto userDto) throws ParseException {
        User user = modelMapper.map(userDto, User.class);
        return user;
    }

    private List<String> convertToIdList(Collection<? extends IdListable> objectsCollection) {
        List<String> dtoList = new ArrayList<>();
        for (IdListable obj: objectsCollection){
            dtoList.add(obj.getId().toString());
        }
        return dtoList;
    }
}
