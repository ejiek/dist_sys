/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.controller;

import com.ejiek.crashSolver.entity.Gender;
import com.ejiek.crashSolver.entity.Injured;
import com.ejiek.crashSolver.entity.InjuredStatus;
import com.ejiek.crashSolver.entity.UserType;
import com.ejiek.crashSolver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class EnumController {
    @Autowired
    UserRepository userRepository;

  @RequestMapping(value = "/rest/enum/user_types")
    public Collection<String> returnActionsStates(){
      Collection<String> list = new ArrayList<>();
      for (UserType type: UserType.values()){
          list.add(type.toString());
      }
        return list;
  }

    @RequestMapping(value = "/rest/enum/genders")
    public Collection<String> returnGenders(){
        Collection<String> list = new ArrayList<>();
        for (Gender gender: Gender.values()){
            list.add(gender.toString());
        }
        return list;
    }

    @RequestMapping(value = "/rest/enum/injured_status")
    public Collection<String> returnInjuredStatus(){
        Collection<String> list = new ArrayList<>();
        for (InjuredStatus status: InjuredStatus.values()){
            list.add(status.toString());
        }
        return list;
    }
}
