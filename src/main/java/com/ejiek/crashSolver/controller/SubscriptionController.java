/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.controller;

import com.ejiek.crashSolver.DTO.SubscriptionDto;
import com.ejiek.crashSolver.entity.*;
import com.ejiek.crashSolver.exceptions.ExistsException;
import com.ejiek.crashSolver.exceptions.ParseException;
import com.ejiek.crashSolver.service.SubscriptionService;
import com.ejiek.crashSolver.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.web.bind.annotation.*;

@RestController
public class SubscriptionController {
    @Autowired
    SubscriptionService subscriptionService;
    @Autowired
    UserService userService;
    @Autowired
    ModelMapper modelMapper;

    @RequestMapping("rest/sub/{id}")
    public SubscriptionDto getSubscribtion(@PathVariable Long id) {
        return convertToDto(subscriptionService.findById(id));
    }

    @RequestMapping(value = "rest/sub")
    public void addSubscription(@RequestBody SubscriptionDto subscriptionDto) {
        subscriptionService.register(convertToEntity(subscriptionDto));
    }

    @RequestMapping(value = "rest/sub/{id}/accidents")
    public List<String> getAccidents(@PathVariable Long id) {
	    Subscription subscription = subscriptionService.findById(id);
        return convertToIdList(subscriptionService.getAccidents(subscription));
    }

    @RequestMapping(value = "rest/sub/{id}/unsubscribe")
    public boolean unsubscribe(@PathVariable Long id) {
        Subscription subscription = subscriptionService.findById(id);
        subscriptionService.delete(subscription);
        return true;
    }

    private SubscriptionDto convertToDto(Subscription subscription) {
        SubscriptionDto subscriptionDto = modelMapper.map(subscription, SubscriptionDto.class);
        subscriptionDto.setUserId(subscription.getUser().getId());
        return subscriptionDto;
    }

    private Subscription convertToEntity(SubscriptionDto subscriptionDto) throws ParseException {
        Subscription subscription = modelMapper.map(subscriptionDto, Subscription.class);
        subscription.setUser(userService.findById(subscriptionDto.getUserId()));
        return subscription;
    }

    private List<String> convertToIdList(Collection<? extends IdListable> objectsCollection) {
        List<String> dtoList = new ArrayList<>();
        for (IdListable obj: objectsCollection){
            dtoList.add(obj.getId().toString());
        }
        return dtoList;
    }
}
