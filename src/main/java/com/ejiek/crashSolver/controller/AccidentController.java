/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.controller;

import com.ejiek.crashSolver.entity.Accident;
import com.ejiek.crashSolver.entity.IdListable;
import com.ejiek.crashSolver.entity.Request;
import com.ejiek.crashSolver.service.AccidentService;
import com.ejiek.crashSolver.service.InjuredService;
import com.ejiek.crashSolver.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class AccidentController {
    @Autowired
    AccidentService accidentService;
    @Autowired
    InjuredService injuredService;
    @Autowired
    RequestService requestService;

    @RequestMapping("rest/accident/{id}")
    public Accident getAccident(@PathVariable Long id) {
        return accidentService.findById(id);
    }

    @RequestMapping(value = "rest/accident/", method = RequestMethod.GET)
    public Accident getAccidentByRequestId(@RequestParam("requestId") Long requestId) {
        Request request = requestService.findById(requestId);
        return accidentService.findByRequest(request);
    }

    @RequestMapping("rest/accident/{id}/injured")
    public Collection<String> getInjured(@PathVariable Long id) {
        Accident accident = accidentService.findById(id);
        return convertToIdList(injuredService.findByAccident(accident));
    }

    private List<String> convertToIdList(Collection<? extends IdListable> objectsCollection) {
        List<String> dtoList = new ArrayList<>();
        for (IdListable obj: objectsCollection){
            dtoList.add(obj.getId().toString());
        }
        return dtoList;
    }

}
