/**
 *MIT License
 *
 *Copyright (c) 2018 ejiek
 *
 *Permission is hereby granted, free of charge, to any person obtaining a copy
 *of this software and associated documentation files (the "Software"), to deal
 *in the Software without restriction, including without limitation the rights
 *to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *copies of the Software, and to permit persons to whom the Software is
 *furnished to do so, subject to the following conditions:
 *
 *The above copyright notice and this permission notice shall be included in all
 *copies or substantial portions of the Software.
 *
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 *SOFTWARE.
 */
package com.ejiek.crashSolver.controller;

import com.ejiek.crashSolver.DTO.InjuredDto;
import com.ejiek.crashSolver.entity.HealthUpdate;
import com.ejiek.crashSolver.entity.IdListable;
import com.ejiek.crashSolver.entity.Injured;
import com.ejiek.crashSolver.exceptions.NotFoundException;
import com.ejiek.crashSolver.exceptions.ParseException;
import com.ejiek.crashSolver.repository.AccidentRepository;
import com.ejiek.crashSolver.repository.HealthUpdateRepository;
import com.ejiek.crashSolver.repository.InjuredRepository;
import com.ejiek.crashSolver.repository.UserRepository;
import com.ejiek.crashSolver.service.InjuredService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
public class InjuredController {
    @Autowired
    InjuredRepository injuredRepository;
    @Autowired
    AccidentRepository accidentRepository;
    @Autowired
    HealthUpdateRepository healthUpdateRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    InjuredService injuredService;
    @Autowired
    ModelMapper modelMapper;

    @RequestMapping(value = "rest/injured/{id}", method = RequestMethod.GET)
    public InjuredDto getInjured(@PathVariable Long id) {
        return convertToDto(injuredService.getInjuredById(id));
    }

    @RequestMapping(value = "rest/injured", method = { RequestMethod.GET, RequestMethod.POST })
    public void registerInjured( @RequestParam("accidentId") Long accidentId,
                                 @RequestBody InjuredDto injuredDto) {
        injuredService.register(convertToEntity(injuredDto), accidentId);
    }

    @RequestMapping(value = "rest/injured", method =  RequestMethod.PUT )
    public void updateInjured( @RequestBody InjuredDto injuredDto) {
        injuredService.update(convertToEntity(injuredDto));
    }

    @RequestMapping("rest/injured/{id}/discharge")
    public void registerInjured( @PathVariable("id") Long id) {
        Injured injured = injuredRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Injured " + id));
        injuredService.discharge(injured);
    }

    @RequestMapping(value = "rest/injured/{id}/accept", method = RequestMethod.GET)
    public void accept(@PathVariable Long id, @RequestParam Long userId) {
        Injured injured = injuredRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Injured " + id));
        injuredService.accept(injured);
    }

    @RequestMapping("rest/injured/{id}/updates")
    public List<String> getHealthUpdates(@PathVariable Long id){
        return convertToIdList(injuredService.healthUpdates(id));
    }

    private InjuredDto convertToDto(Injured injured) {
        InjuredDto injuredDto = modelMapper.map(injured, InjuredDto.class);
        injuredDto.setMedic(injured.getMedic());
        return injuredDto;
    }

    private Injured convertToEntity(InjuredDto injuredDto) throws ParseException {
        Injured injured = modelMapper.map(injuredDto, Injured.class);
        injured.setMedic(injuredDto.getMedicConverted(userRepository));

        return injured;
    }

    private List<String> convertToIdList(Collection<? extends IdListable> objectsCollection) {
        List<String> dtoList = new ArrayList<>();
        for (IdListable obj: objectsCollection){
            dtoList.add(obj.getId().toString());
        }
        return dtoList;
    }
}
